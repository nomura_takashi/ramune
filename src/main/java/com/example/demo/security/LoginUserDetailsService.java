package com.example.demo.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

/***
 * ログインイン時に認証ユーザーを「employeeテーブル」から情報を取得するクラス
 */
@Service
public class LoginUserDetailsService implements UserDetailsService {


    @Autowired
    UserRepository userRepository;
    @Autowired
    private HttpServletRequest request;



    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        //入力された名前をキーにemployeeテーブルのレコードを1件取得
        User user = userRepository.findUser(name);

        //該当レコードが取得できなかった場合はエラーにする
        if  (user  ==  null )   {
            throw new AuthenticationCredentialsNotFoundException("アカウントまたはパスワードが誤っています");
        }

        if(user.getIs_stopped() == 1) {
        	throw new LockedException("アカウントまたはパスワードが誤っています");
        }



        String role;
        //ログインユーザー権限を設定
        if(user.getDepartment_id()==1) {
        	role = "ROLE_ADMIN";
        }else {
        	role = "ROLE_GENERAL";
        }
        user.setRole(role);
        return new LoginUserDetails(user, role);
    }
}
