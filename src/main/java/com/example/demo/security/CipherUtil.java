package com.example.demo.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

/**
 * 暗号化ユーティリティー
 */
public class CipherUtil {

	//暗号化
		public static String encrypt(String target) {

		try {
				MessageDigest md = MessageDigest.getInstance("SHA-256");
				md.update(target.getBytes());
				return Base64.encodeBase64URLSafeString(md.digest());
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
	 	}
}
