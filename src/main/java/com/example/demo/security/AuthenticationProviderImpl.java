package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {

	@Autowired
	LoginUserDetailsService loginUserDetailsService;



	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		authentication.isAuthenticated(); // こ䛾時点で䛿 false;
		// 入力されたユーザー名とパスワードを取得。
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();
		if (username.isEmpty() && password.isEmpty()){
			String errorMessage = "アカウントが入力されていません" + "\n"+"パスワードが入力されていません";
			throw new AuthenticationCredentialsNotFoundException(errorMessage);
		}
		if (username.isEmpty() ){
				throw new AuthenticationCredentialsNotFoundException("アカウントが入力されていません");
		}
		if (password.isEmpty() ){
			throw new AuthenticationCredentialsNotFoundException("パスワードが入力されていません");
		}

		UserDetails user = loginUserDetailsService.loadUserByUsername(username);
		String encodPassword = CipherUtil.encrypt(password);
		if(!user.getPassword().equals(encodPassword)) {
			throw new AuthenticationCredentialsNotFoundException("アカウントまたはパスワードが誤ってます");
		}
		UsernamePasswordAuthenticationToken authenticationResult
		= new UsernamePasswordAuthenticationToken(user,authentication.getCredentials(),user.getAuthorities());

		authenticationResult.setDetails(authentication.getDetails());
		authenticationResult.isAuthenticated();
				return authenticationResult;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication);
	}

}
