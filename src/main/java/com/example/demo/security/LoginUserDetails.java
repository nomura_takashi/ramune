package com.example.demo.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class LoginUserDetails extends User {
  //employeeテーブルから取得したオブジェクトを格納
  private final com.example.demo.entity.User user;

  //認証処理
  public LoginUserDetails(com.example.demo.entity.User user, String role) {
      //employeeテーブルの名前とパスワードでログイン認証を行う
      super(user.getAccount(), user.getPassword(), AuthorityUtils.createAuthorityList(role));

      this.user = user;
  }

  public com.example.demo.entity.User getUserName() {
	  return this.user;
  }
}
