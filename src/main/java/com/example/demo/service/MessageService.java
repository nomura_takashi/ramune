package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService {
	@Autowired
	MessageRepository messageRepository;

	// レコード全件取得
	public List<Message> findAllMessage() {
		return messageRepository.findAll();
	}

	// レコード追加
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	//レコード削除
	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id);
	}

	public Message findMessage(Integer id) {
		Message message = (Message) messageRepository.findById(id).orElse(null);
		return message;
	}

	//メッセージ日付絞り込み
	public List<Message> choiceMessage(String start, String end, String category) {
		String stDate = null;
		String edDate    = null;
		if (!start.isEmpty()) {
			stDate = start + " 00:00:00";
		} else {
			stDate = "2020-01-01 00:00:00";
		}

		if (!end.isEmpty()) {
			edDate = end + " 23:59:59";
		} else {
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar calendar = Calendar.getInstance();
			edDate = sdFormat.format(calendar.getTime());
		}

		/*if (start.isEmpty() && end.isEmpty() && !category.isEmpty()) {
			return messageRepository.categoryRefinement(category);
		}*/

		Timestamp startDate = Timestamp.valueOf(stDate);
		Timestamp endDate    = Timestamp.valueOf(edDate);
		SimpleDateFormat sodFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		sodFormat.format(startDate);
		sodFormat.format(endDate);

		if (category.isEmpty()) {
			return messageRepository.dateRefinement(startDate, endDate);
		}
		return messageRepository.allRefinement(startDate, endDate, category);
	}
/*
	//メッセージカテゴリー絞り込み
	public List<Message> categoryMessage(String category) {
		if (category.isEmpty()) {
			return messageRepository.categoryRefinement(category);
		}
	}*/
}
