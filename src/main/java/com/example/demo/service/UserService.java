package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.CipherUtil;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	//アカウントの全件取得
	public List<User> findAllUser() {
		return userRepository.findAll();
	}

	//アカウントの検索
	public User findUser(String account, String password) {
		password = CipherUtil.encrypt(password);
		return userRepository.findUser(account,password);
	}

	//アカウントの検索(id)
	public User findUser(int id) {
		User user = (User) userRepository.findById(id).orElse(null);
		return user;
	}


	//アカウントの登録
	public void  saveUser(User user) {
		//暗号化
		user.setPassword(CipherUtil.encrypt(user.getPasswordEntity()));
		userRepository.save(user);
	}

	//アカウントのアップデート
	public void updateUser(User user) {
		//パスワード入力が空なら元のパスワードと同じにする
		if(user.getPasswordEntity().isEmpty()) {
			User mainUser = userRepository.findUser(user.getId());
			user.setPassword(mainUser.getPassword());
		}else {
			user.setPassword(CipherUtil.encrypt(user.getPasswordEntity()));
		}
		userRepository.save(user);
	}

	//エラーのチェック
	public List<String> isValid(User user) {
		List<String> errorMessageList = new ArrayList<>();
		User users = userRepository.findUser(user.getAccount());

		if(user.getPasswordEntity().isEmpty()) {
			errorMessageList.add("パスワードを入力してください");
		}

		//空じゃないときの文字数チェック
		if(!user.getPasswordEntity().isEmpty()) {
			//6文字以下じゃないか？
			if(user.getPasswordEntity().length() < 6) {
				errorMessageList.add("パスワードは6文字以上で入力してください");
			}
			//20字以上じゃないか？
			if(user.getPasswordEntity().length() >= 20) {
				errorMessageList.add("パスワードは20文字以下で入力してください");
			}
			//パスワードと確認用パスワードが一致しているかチェック
			if(!(user.getPasswordEntity().equals(user.getConfirmPassword()))) {
				errorMessageList.add("入力したパスワードと確認用パスワードが一致しません");
			}
			//重複していないかチェック
			if(users != null && users.getId() != user.getId()) {
				errorMessageList.add("アカウントが重複しています");
			}
		}
		return errorMessageList;
	}


	public List<String> updateIsValid(User user) {
		List<String> errorMessageList = new ArrayList<>();
		User users = userRepository.findUser(user.getAccount());

		//重複していないかチェック
		if(users != null && users.getId() != user.getId()) {
			errorMessageList.add("アカウントが重複しています");
		}

		//空じゃないときの文字数チェック
		if(!user.getPasswordEntity().isEmpty()) {
			//6文字以下じゃないか？
			if(user.getPasswordEntity().length() < 6) {
				errorMessageList.add("パスワードは6文字以上で入力してください");
			}
			//20字以上じゃないか？
			if(user.getPasswordEntity().length() >= 20) {
				errorMessageList.add("パスワードは20文字以下で入力してください");
			}
			//パスワードと確認用パスワードが一致しているかチェック
			if(!(user.getPasswordEntity().equals(user.getConfirmPassword()))) {
				errorMessageList.add("入力したパスワードと確認用パスワードが一致しません");
			}
		}
		return errorMessageList;
	}
}
