package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	// レコード全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll();
	}

	// レコード追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	public Comment findEditComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	//レコード削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}