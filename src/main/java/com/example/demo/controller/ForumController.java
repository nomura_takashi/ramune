package com.example.demo.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.security.LoginUserDetails;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;


@Controller
public class ForumController {
	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	@Autowired
	UserService userService;
	@Autowired
	HttpSession session;

	List<String> search = new ArrayList<>();

	// 投稿内容表示画面

	@GetMapping("/ramune")
	public ModelAndView index(Model model, @AuthenticationPrincipal LoginUserDetails userDetails,RedirectAttributes redirectAttributes) {

		if(userDetails == null) {
			String errorMessage = "ログインしてください";
			redirectAttributes.addFlashAttribute("notAccess", errorMessage);
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();
		String error = (String) model.getAttribute("key1");
		User user = userDetails.getUser();
		search.add("");
		search.add("");
		search.add("");

		//返信用の空のentityを準備
		Comment comment = new Comment();

		// 投稿を全件取得
		List<User> userData = userService.findAllUser();
		List<Message> messageData = messageService.findAllMessage();
		List<Comment> commentData   = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("users", userData);
		mav.addObject("loginUser", user);
		mav.addObject("error", error);
		mav.addObject("search", search);
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("commentForm", comment);
		return mav;
	}

	//トップページ
	@GetMapping("/top")
	public ModelAndView top(Model model, @AuthenticationPrincipal LoginUserDetails userDetails) {

		ModelAndView mav = new ModelAndView();
		String error = (String) model.getAttribute("key1");
		User user = userDetails.getUser();
		//返信用の空のentityを準備
		Comment comment = new Comment();

		// 投稿を全件取得
		List<User> userData = userService.findAllUser();
		List<Message> messageData = messageService.findAllMessage();
		List<Comment> commentData   = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("users", userData);
		mav.addObject("loginUser", user);
		mav.addObject("error", error);
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("commentForm", comment);
		return mav;
	}

	// 新規投稿画面

	@GetMapping("/new")
	public ModelAndView newMessage(@AuthenticationPrincipal LoginUserDetails userDetails,RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView();


		if(userDetails == null) {
			String errorMessage = "ログインしてください";
			redirectAttributes.addFlashAttribute("notAccess", errorMessage);
			return new ModelAndView("redirect:/login");
		}
		User user = userDetails.getUser();
		// form用の空のentityを準備
		Message message = new Message();



		message.setUser_id(user.getId());
		// 画面遷移先を指定
		mav.setViewName("/new");

		// 準備した空のentityを保管
		mav.addObject("formModel", message);
		return mav;
	}

	// 投稿処理
	@PostMapping("/new")

	public ModelAndView addMessage(@ModelAttribute("formModel") @Validated Message message, BindingResult result,ModelAndView model) {
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/new");
			return mav;
		}


		// 投稿をテーブルに格納
		messageService.saveMessage(message);
		// rootへリダイレクト

		return new ModelAndView("redirect:/ramune");
	}

	//投稿削除
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteMessage(@PathVariable Integer id,RedirectAttributes redirectAttributes) {

		Message message = messageService.findMessage(id);
		if(message == null) {
			String errorMessage = "不正なパラメータです";
			redirectAttributes.addFlashAttribute("notAccess", errorMessage);
			return new ModelAndView("redirect:/ramune");
		}


		messageService.deleteMessage(id);

		return new ModelAndView("redirect:/");
	}


	@GetMapping("/edit/{id}")
	public ModelAndView editMessage(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		Message message = messageService.findMessage(id);

		mav.setViewName("/edit");
		mav.addObject("formModel", message);
		return mav;
	}

	@PutMapping("/update/{id}")
	public ModelAndView updateMessage(@PathVariable Integer id, @ModelAttribute("formModel") Message message) {
		messageService.saveMessage(message);
		return new ModelAndView("redirect:/");
	}






	//コメント投稿処理
	@PostMapping("/commentAdd")
	public ModelAndView addContent(@Validated @ModelAttribute("commentForm") Comment comment, BindingResult result,Model model, @AuthenticationPrincipal LoginUserDetails userDetails) {
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			User user = userDetails.getUser();

			// 投稿を全件取得
			List<User> userData = userService.findAllUser();
			List<Message> messageData = messageService.findAllMessage();
			List<Comment> commentData   = commentService.findAllComment();

			// 画面遷移先を指定
			mav.setViewName("/top");

			// 投稿データオブジェクトを保管
			mav.addObject("users", userData);
			mav.addObject("loginUser", user);
			mav.addObject("search", search);
			mav.addObject("messages", messageData);
			mav.addObject("comments", commentData);
			mav.addObject("commentForm", comment);
			return mav;
		}
		Message message = messageService.findMessage(comment.getMessage_id());
		message.setUpdated_date(comment.getUpdated_date());
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		// rootへリダイレクト

		return new ModelAndView("redirect:/ramune");
	}









	//投稿削除
	@DeleteMapping("/commentDelete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);

		return new ModelAndView("redirect:/ramune");
	}


		//投稿絞り込み
	@GetMapping("/search")
	public ModelAndView dateRefinement(@RequestParam("start") String start, @RequestParam("end") String end, @RequestParam("category") String category,  @AuthenticationPrincipal LoginUserDetails userDetails) {
		ModelAndView mav = new ModelAndView();
		List<Message> messagesData = messageService.choiceMessage(start, end, category);
		List<User> userData = userService.findAllUser();
		List<Comment> commentData   = commentService.findAllComment();

		User user = userDetails.getUser();
		search.set(0, start);
		search.set(1,end);
		search.set(2,category);
		//返信用の空のentityを準備
		Comment comment = new Comment();
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("loginUser", user);
		mav.addObject("users", userData);
		mav.addObject("search", search);
		mav.addObject("messages", messagesData);
		mav.addObject("comments", commentData);
		mav.addObject("commentForm", comment);
		return mav;
	}
}
