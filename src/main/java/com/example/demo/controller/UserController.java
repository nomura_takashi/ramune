package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.security.LoginUserDetails;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
public class UserController {
	@Autowired
	UserService userService;
	@Autowired
	BranchService branchService;
	@Autowired
	DepartmentService departmentService;
	@Autowired
	HttpSession session;




	//singup画面表示
	@GetMapping("/signup")
	public ModelAndView getSignUp(Model model,@AuthenticationPrincipal LoginUserDetails userDetails,RedirectAttributes redirectAttributes) {
		if(userDetails == null) {
			String errorMessage = "ログインしてください";
			redirectAttributes.addFlashAttribute("notAccess", errorMessage);
			return new ModelAndView("redirect:/login");
		}
		User loginUser = userDetails.getUser();
		if(loginUser.getRole().equals("ROLE_GENERAL")) {

			String errorMessage = "権限がありません";
			redirectAttributes.addFlashAttribute("key1", errorMessage);
			return new ModelAndView("redirect:/ramune");
		}

		ModelAndView mav = new ModelAndView();
		User user = new User();
		List<Branch> branchData = branchService.findAllBranch();
		List<Department> departmentData = departmentService.findAllDepartment();

		mav.setViewName("/signup");
		//投稿データオブジェクトを保管
		mav.addObject("SignUpForm", user);
		mav.addObject("branchData", branchData);
		mav.addObject("departmentData", departmentData);

        return mav;
	}



	//アカウント登録処理
	@PostMapping("/signup")
	public ModelAndView SignUp(Model model,@ModelAttribute("SignUpForm") @Validated User user,BindingResult result,RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView();
		List<String>errorMessageList = userService.isValid(user);
		if(errorMessageList.size()>0 || result.hasErrors() ) {
			mav.addObject("errorMessageList", errorMessageList);
			List<Branch> branchData = branchService.findAllBranch();
			List<Department> departmentData = departmentService.findAllDepartment();
			mav.setViewName("/signup");
			mav.addObject("branchData", branchData);
			mav.addObject("departmentData", departmentData);
			return mav;
		}
		userService.saveUser(user);
		return new ModelAndView("redirect:/userManagement");
	}

	@GetMapping("/userEdit")
	public ModelAndView getUserEdit(RedirectAttributes redirectAttributes) {
		String errorMessage = "不正なパラメータです";
		redirectAttributes.addFlashAttribute("notAccess", errorMessage);
		return new ModelAndView("redirect:/userManagement");

	}


	@GetMapping("/userEdit/{id}")
	public ModelAndView getUserEdit(@PathVariable String id, @AuthenticationPrincipal LoginUserDetails userDetails,RedirectAttributes redirectAttributes) {

		if(id.isEmpty() || !id.matches("^[0-9]+$") || userService.findUser(Integer.parseInt(id)) == null ) {

				String errorMessage = "不正なパラメータです";
				redirectAttributes.addFlashAttribute("notAccess", errorMessage);
				return new ModelAndView("redirect:/userManagement");

		}


		if(userDetails == null) {
			String errorMessage = "ログインしてください";
			redirectAttributes.addFlashAttribute("notAccess", errorMessage);
			return new ModelAndView("redirect:/login");
		}
		User loginUser = userDetails.getUser();
		if(loginUser.getRole().equals("ROLE_GENERAL")) {

			String errorMessage = "権限がありません";
			redirectAttributes.addFlashAttribute("key1", errorMessage);
			return new ModelAndView("redirect:/ramune");
		}
		ModelAndView mav = new ModelAndView();


		User user = userService.findUser(Integer.parseInt(id));
		List<Branch> branchData = branchService.findAllBranch();
		List<Department> departmentData = departmentService.findAllDepartment();
		mav.setViewName("/userEdit");

		//投稿データオブジェクトを保管
		mav.addObject("loginUser", loginUser);
		mav.addObject("branchData", branchData);
		mav.addObject("departmentData", departmentData);
		mav.addObject("formModel", user);
		return mav;
	}

	//ユーザー編集処理
	@PutMapping("/userUpdate/{id}")
	public ModelAndView updateUserEdit(@PathVariable Integer id, @ModelAttribute("formModel") @Validated User user,BindingResult result,
			RedirectAttributes redirectAttributes,@AuthenticationPrincipal LoginUserDetails userDetails) {




		User loginUser = userDetails.getUser();
		ModelAndView mav = new ModelAndView();
		mav.addObject("loginUser", loginUser);
		List<String>errorMessageList = userService.updateIsValid(user);
		if(errorMessageList.size() > 0 || result.hasErrors() ) {
			mav.addObject("errorMessageList", errorMessageList);
			List<Branch> branchData = branchService.findAllBranch();
			List<Department> departmentData = departmentService.findAllDepartment();
			mav.setViewName("/userEdit");
			mav.addObject("branchData", branchData);
			mav.addObject("departmentData", departmentData);
			return mav;
		}

		userService.updateUser(user);
		return new ModelAndView("redirect:/userManagement");
	}

	//ユーザー管理画面表示
	@GetMapping("/userManagement")
	public ModelAndView userManagement(@AuthenticationPrincipal LoginUserDetails userDetails,RedirectAttributes redirectAttributes,Model model) {
		if(userDetails == null) {
			String errorMessage = "ログインしてください";
			redirectAttributes.addFlashAttribute("notAccess", errorMessage);
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();
		User user = userDetails.getUser();


		String error = (String) model.getAttribute("notAccess");
		mav.addObject("error", error);

		//権限のチェック、権限がない場合はリダイレクトしエラーメッセージ表示
		if(user.getRole().equals("ROLE_GENERAL")) {

			String errorMessage = "権限がありません";
			redirectAttributes.addFlashAttribute("key1", errorMessage);
			return new ModelAndView("redirect:/ramune");
		}
		List<User> userData = userService.findAllUser();
		List<Branch> branchData = branchService.findAllBranch();
		List<Department> departmentData = departmentService.findAllDepartment();
		mav.setViewName("/userManagement");
		mav.addObject("loginUser", user);
		mav.addObject("users", userData);
		mav.addObject("branchData", branchData);
		mav.addObject("departmentData", departmentData);
		return mav;
	}

	//ユーザー有効無効処理
	@PutMapping("/switchAccount/{id}")
	public ModelAndView swichAccount(@PathVariable Integer id) {
		User user = userService.findUser(id);
		if (user.getIs_stopped() == 0) {
			user.setIs_stopped(1);
		} else {
			user.setIs_stopped(0);
		}
		userService.saveUser(user);
		return new ModelAndView("redirect:/userManagement");
	}
	@GetMapping("/login")
	public ModelAndView getLogin(Model model) {
		ModelAndView mav = new ModelAndView();
		String error = (String) model.getAttribute("notAccess");
		mav.setViewName("/login");
		mav.addObject("error", error);
        return mav;
    }	@PostMapping("/login")
    String postLogin() {
        return "/";
    }

}
