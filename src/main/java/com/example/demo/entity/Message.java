package com.example.demo.entity;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.UpdateTimestamp;


@Entity
@Table(name = "messages")
public class Message {
	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank(message = "件名を入力してください")
	@Size(min=0,max=30,message="件名は30文字以下で入力してください")
	@Column
	private String title;

	@NotBlank(message = "本文を入力してください")
	@Size(min=0,max=1000,message="本文は1000文字以下で入力してください")
	@Column
	private String text;

	@NotBlank(message = "カテゴリを入力してください")
	@Size(min=0,max=10,message="カテゴリは10文字以下で入力してください")
	@Column
	private String category;

	@Column
	private int user_id;





	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@UpdateTimestamp
	@Column(nullable = false)
	private Timestamp updated_date;

	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

	public int getId() {
	return id;
	}

	public void setId(int id) {
	this.id = id;
	}




}

