package com.example.demo.entity;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.UpdateTimestamp;
@Entity
@Table(name = "comments")
public class Comment {
	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank(message = "本文を入力してください")
	@Size(min=0,max=500,message="本文は500文字以下で入力してください")
	@Column
	private String text;

	@Column
	private int user_id;

	@Column
	private int message_id;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getMessage_id() {
		return message_id;
	}

	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}

	@UpdateTimestamp
	@Column(nullable = false)
	private Timestamp updated_date;

	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

	public int getId() {
	return id;
	}

	public void setId(int id) {
	this.id = id;
	}
}

