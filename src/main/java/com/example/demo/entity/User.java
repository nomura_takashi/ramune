package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.example.demo.validation.CheckAnnotation;

@CheckAnnotation
@Table(name="users")
@Entity
public class User{
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank(message="アカウント名を入力してください")
	@Length(min =6 ,message="アカウント名は6文字以上で入力してください")
	@Size(max=20,message="アカウント名は20文字以下で入力してください")
	@Column
	private String account;




	private String passwordEntity;


	@Column
	private String password;




	private String confirmPassword;

	@NotBlank(message="ユーザー名を入力してください")
	@Size(max=10,message="ユーザー名は10文字以下で入力してください")
	@Column
	private String name;


	@Column
	private int branch_id;
	@Column
	private int department_id;

	@Column
	private int is_stopped;

	private String role;

	private String updatePassword;

	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	public int getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}

	public int getIs_stopped() {
		return is_stopped;
	}

	public void setIs_stopped(int is_stopped) {
		this.is_stopped = is_stopped;
	}

	public String getPasswordEntity() {
		return passwordEntity;
	}

	public void setPasswordEntity(String passwordEntity) {
		this.passwordEntity = passwordEntity;
	}

	public String getUpdatePassword() {
		return updatePassword;
	}


	public void setUpdatePassword(String updatePassword) {
		this.updatePassword = updatePassword;
	}

}
