package com.example.demo.entity;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class LoginUser {
	@NotBlank(message="アカウント名を入力してください")
	@Size(max=20,message="アカウント名は20文字以下で入力してください")
	@Size(min=6,message="アカウント名は6文字以上で入力してください")
	@Column
	private String account;



	@NotBlank(message="パスワードを入力してください")
	@Size(min=6,message="パスワードは6文字以上で入力してください")
	@Size(max=20,message="パスワードは20文字以下で入力してください")
	private String passwordEntity;



	public String getAccount() {
		return account;
	}



	public void setAccount(String account) {
		this.account = account;
	}



	public String getPasswordEntity() {
		return passwordEntity;
	}



	public void setPasswordEntity(String passwordEntity) {
		this.passwordEntity = passwordEntity;
	}
}
