package com.example.demo.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
// 1で記載した入力チェック内容を記載したクラスをかく
@Constraint(validatedBy = {AnnotationValidation.class})
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
// クラス名には、アノテーション名として使いたい名前をつける
public @interface CheckAnnotation {

  // 入力チェック不可だった場合に、表示するエラーメッセージ
  String message() default "支社名と部署名の組み合わせが不正です。";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

  @Target({ElementType.TYPE})
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  public @interface List {
    CheckAnnotation[] value();
  }
}
