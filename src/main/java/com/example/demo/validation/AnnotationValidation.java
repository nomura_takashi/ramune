package com.example.demo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

// ConstraintValidator の実装クラスとして作成する
// <>内の引数1つ目は下記2のアノテーションクラス名
public class AnnotationValidation
    implements ConstraintValidator<CheckAnnotation, Object> {

  // バリデーション対象の変数の値を入れる
  private String b_id;
  private String d_id;
  // アノテーションクラス（下記2）で設定しているエラーメッセージが入る
  private String message;

  /**
   * 初期化処理
   * ()内のクラスは下記2のアノテーションクラス
   */
  public void initialize(CheckAnnotation annotation) {
    this.b_id = "branch_id";
    this.d_id = "department_id";
    this.message = annotation.message();
  }

  /**
   *  自分でバリデーション内容を設定する
   */
  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {
//  指定されたオブジェクトの新しいBeanWrapperImplを作成
	  BeanWrapper beanWrapper = new BeanWrapperImpl(value);

	  Object branchId = beanWrapper.getPropertyValue(this.b_id);
      Object departmentId = beanWrapper.getPropertyValue(this.d_id);

      if(branchId.equals(1)) {
    	  if(departmentId.equals(1) || departmentId.equals(2)) {
    		  return true;
    	  } else {
    		  context.disableDefaultConstraintViolation();//contextのデフォルトのメッセージの無効化
    		  context.buildConstraintViolationWithTemplate(message).addPropertyNode("branch_id").addConstraintViolation();
    		  return false;
    	  }
      } else if(branchId.equals(2) || branchId.equals(3) || branchId.equals(4)) {
    	  if(departmentId.equals(3) || departmentId.equals(4)) {
    		  return true;
    	  } else {
    		  context.disableDefaultConstraintViolation();//contextのデフォルトのメッセージの無効化
    		  context.buildConstraintViolationWithTemplate(message).addPropertyNode("branch_id").addConstraintViolation();
    		  return false;
    	  }
      }
      return false;
  }
}


/**/
