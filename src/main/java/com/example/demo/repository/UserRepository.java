package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	@Query("select t from User t WHERE t.account = ?1 AND t.password = ?2")
	User findUser(String account,String password);

	@Query("select t from User t WHERE t.account = ?1")
	User findUser(String account);

	@Query("select t from User t WHERE t.id = ?1")
	User findUser(int id);

}
