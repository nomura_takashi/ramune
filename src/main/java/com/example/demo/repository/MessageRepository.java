package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Message;
@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
	@Query("select t from Message t  order by updated_date desc ")
	List<Message> findAll();


	@Query("select t from Message t WHERE t.updated_date BETWEEN ?1 AND ?2 order by updated_date desc ")
	List<Message> dateRefinement(Timestamp startDate, Timestamp endDate);

	@Query("select t from Message t WHERE t.category LIKE %?1% order by updated_date desc ")
	List<Message> categoryRefinement(String category);

	@Query("select t from Message t WHERE t.updated_date BETWEEN ?1 AND ?2 AND t.category LIKE %?3% order by updated_date desc ")
	List<Message> allRefinement(Timestamp startDate, Timestamp endDate, String category);

}
